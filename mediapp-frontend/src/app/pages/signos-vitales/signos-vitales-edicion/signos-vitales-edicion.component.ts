import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable, switchMap } from 'rxjs';
import { Paciente } from 'src/app/_model/paciente';
import { Signos } from 'src/app/_model/signos';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignosService } from 'src/app/_service/signos.service';

@Component({
  selector: 'app-signos-vitales-edicion',
  templateUrl: './signos-vitales-edicion.component.html',
  styleUrls: ['./signos-vitales-edicion.component.css']
})
export class SignosVitalesEdicionComponent implements OnInit {
  pacientes$: Observable<Paciente[]>;
  idPacienteSeleccionado: number;
  edicion: boolean = false;
  id: number = 0;
  form: FormGroup;
  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();


  constructor(
    private pacienteService: PacienteService,
    private signosService: SignosService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'temperatura': new FormControl(''),
      'ritmo': new FormControl(''),
      'pulso': new FormControl(''),
      'fecha': new FormControl(''),
      'idPaciente': new FormControl('')
    });
    this.route.params.subscribe(data => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });
    this.listarPacientes();
  }

  listarPacientes() {

    this.pacientes$ = this.pacienteService.listar();
  }
  operar() {
    let pac = new Paciente;
    pac.idPaciente = this.idPacienteSeleccionado;
    let signos = new Signos();
    signos.idSignos = this.form.value['id'];
    signos.temperatura = this.form.value['temperatura'];
    signos.ritmo = this.form.value['ritmo'];
    signos.pulso = this.form.value['pulso'];
    signos.fecha = moment(this.fechaSeleccionada).format('YYYY-MM-DDTHH:mm:ss');
    signos.paciente = pac;


    if (this.edicion) {
      //MODFICAR
      //FORMA IDEAL
      this.signosService.modificar(signos).pipe(switchMap( () => {
        return this.signosService.listar();
      }))
      .subscribe(data => {
        this.signosService.setSignosCambio(data);
          this.signosService.setMensajeCambio('SE MODIFICO');
      });

      /*this.pacienteService.modificar(paciente).subscribe(() => {
        this.pacienteService.listar().subscribe(data => {
          this.pacienteService.pacienteCambio.next(data);
          this.pacienteService.mensajeCambio.next('SE MODIFICO');
        });
      });*/
    } else {
      //REGISTRAR
      this.signosService.registrar(signos).subscribe(() => {
        this.signosService.listar().subscribe(data => {
          this.signosService.setSignosCambio(data);
          this.signosService.setMensajeCambio('SE REGISTRO');
        });
      });
    }
    this.router.navigate(['/pages/signos-vitales']);

  }

  initForm() {
    if (this.edicion) {
      this.signosService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idSignos),
          'temperatura': new FormControl(data.temperatura),
          'ritmo': new FormControl(data.ritmo),
          'pulso': new FormControl(data.pulso),
          'fecha': new FormControl(data.fecha),
          idPacienteSeleccionado: new FormControl(data.paciente.idPaciente),

        });
      });
    }
  }

}
