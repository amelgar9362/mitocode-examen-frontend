import { Paciente } from "./paciente";

export class Signos {
  idSignos: number;
  temperatura: string;
  ritmo: string;
  pulso: string;
  fecha: string;
  paciente: Paciente;

}
